# saltpay-tech-challenge

# GitLab 
#### Clone the repo using this command:
    git clone https://{yourGitLabUserName}@gitlab.com/ShafiaShahid/saltpay-tech-challenge.git

NOTE: You will need to have access to this repo in order to clone it.


# Configurations
Kindly download the .env emailed to you and paste it to the root directory. Your code structure should be like:

    saltpay-tech-challenge
          |
          .
          .
          |__config
          |__.env
          |__index.js
          .
          .
          .

 # Database

  MongoDB is being used locally for this project. Before starting, make sure you have mongo installed. Also make sure you run your mongod executable file by following these steps:
 - Open command prompt and change directory to the Bin folder of your Mongo directory. (Check C:\Program Files\MongoDB)
 - Run `mongod`

 Keeping this cmd running in the background, continue with the following steps to set up the database:
 ## Creating a Database
 Although you can use the <strong>cmd</strong> for this, using the GUI is the preferred way because depending on your version of mongo, there might be some issues with the sample CSV provided.

  ### Using GUI 
 - Open your MongoDB Compass Community 
 - Click create Database
 - Name your db 'OpenFoodFacts' and your collection 'Products'.

 Since you are going to be connecting to this db, make sure you type the exact name (case sensitive).
 In case you want to change the db or the collection name, make sure you update the .env file of this project accordingly. 

 ### Using cmd
 - Open cmd
 - First run `mongo`
 - Now run `use OpenFoodFacts` 
 This will create a db name OpenFoodFacts.
 #### Importing CSV to database 
  - Download the sample CSV included in the email
  - Open another command prompt and relocate to the Bin folder of your mongo again
  - Run ` mongoimport -d OpenFoodFacts -c Products --type CSV --file openfoodfacts.csv --headerline`
    
    Where:  
    * OpenFoodFacts -> databaseName 
    * products -> collectionName
    * type is the file type(CSV in this case)
    * file specifies the filename (en.openfoodfacts.org.products.csv in our case)
    * headerline is the header of the CSV, which is already specified in the same
    
<i>To make sure everyhting so far is up to track, open a new cmd run `mongo` followed by `show dbs`. This should list all the DBs you have along with the newly crated DB <b>OpenFoodFacts.</i></b>
    
## Starting the Server
____
- After cloning the repo, open your terminal.
- Relocate to the folder you cloned the repo to.
- Install dependencies by running `npm i`
- Run `npm start`

