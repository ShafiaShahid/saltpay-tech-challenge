import Joi from 'joi';
import path from 'path';
import { env } from 'process';

// require and configure dotenv, will load vars in .env in PROCESS.ENV
require('dotenv').config(path.resolve(__dirname, '../../.env'));

// define validation for all the env vars
const envVarsSchema = Joi.object({
    NODE_ENV: Joi.string()
        .allow(['development', 'production', 'test', 'provision'])
        .default('development'),
    PORT: Joi.number()
        .default(8080),
    URL: Joi.string()
        .default('mongodb://localhost:27017'),
    DATABASE_NAME: Joi.string()
        .default('OpenFoodFacts'),
    COLLECTION_NAME: Joi.string()
        .default('Products'),
    OFF_URL: Joi.string()
        .default('https://world.openfoodfacts.org')
}).unknown()
    .required();

const { error, value: envVars } = Joi.validate(process.env, envVarsSchema);
if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}

const config = {
    env: envVars.NODE_ENV,
    port: envVars.PORT,
    url: envVars.URL,
    dbName: envVars.DATABASE_NAME,
    collectionName: envVars.COLLECTION_NAME,
    OFF_URL: envVars.OFF_URL
};

export default config;
