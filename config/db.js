import mongoose from 'mongoose'

// importing env variables
import config from './config'

// Connecting to local mongoDB
mongoose.connect(`mongodb://localhost/${config.dbName}`, { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true });

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log("connected to mongoDB")
});