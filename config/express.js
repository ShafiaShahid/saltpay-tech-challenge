import express from 'express'
import cors from 'cors';

// importing routes
import routes from '../routes/index.routes'

// start app
const app = express()

// init middleware
app.use(express.json( { extended: false }))
app.use(cors({
	origin: ['http://localhost:3000']
}))
// routes
app.use('/api', routes)

export default app;