/**
 * This is a custom helper for sending consistent API response, avoiding redundant code
 */

const apiRes = {};
 
apiRes.success = async (res, dataCount, totalDataCount, data, msg = 'Success') => {
    res.status(200).json({
        msg,
        dataCount,
        totalDataCount,
        data
    });
}

apiRes.notFound = async (res, msg = 'Not found') => {
    res.status(404).json({
        msg
    });
}

apiRes.unauthorised = async (res, msg = 'Unauthorised.') => {
    res.status(401).json({
        msg
    });
}

apiRes.forbidden = async (res, msg = 'Not allowed') => {
    res.status(403).json({
        msg
    });
}
apiRes.serverError = async (res, msg = 'Something went wrong, try again') => {
    res.status(500).json({
        msg
    });
}


module.exports = apiRes; 