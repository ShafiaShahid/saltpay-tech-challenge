import config from './config/config';
import app from './config/express';

if (!module.parent) {
    // start listening on PORT defined in env (default 8080)
    app.listen(config.port, () => {
        /* eslint no-console: ["error", { allow: ["info"] }] */
        console.info(`server started on port ${config.port} (${config.env})`);
    });
}

export default app;