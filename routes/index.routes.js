import express from 'express';

import v1 from './v1/v1.routes';
import v2 from './v2/v2.routes';

const router = express.Router();

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) => {
    res.send('OK');
});

// Maintaining routes for different versions
router.use('/v1', v1);
router.use('/v2', v2);


export default router;
