import express from 'express'
import config from '../../config/config'
import apiRes from '../../helpers/apiRes'

const MongoClient = require('mongodb').MongoClient;

const router = express.Router();

// @route    GET api/products
// @desc     Get all products
// @access   public
router.get('/', async (req, res) => {

    // For pagination of results
    let pageOptions = {
        page: parseInt(req.query.page, 10) || 0,
        limit: parseInt(req.query.limit, 10) || 12
    },
        searchOptions = req.query.search || undefined,
        filterOptions = req.query.country || undefined,
        options = {}

    try {
        // Using connect method to connect to the server
        MongoClient.connect(config.url, async function (err, client) {
            const db = client.db(config.dbName)
            let totalCount

            // For searching for products by name
            if (searchOptions != undefined) {
                options.product_name = { '$regex': searchOptions, '$options': 'i' }
            }
            // For filtering products by Country
            if (filterOptions != undefined) {
                options.countries = { '$regex': filterOptions, '$options': 'i' }
            }


            let result = db.collection('Products').find(options)
                .skip(pageOptions.page * pageOptions.limit)
                .limit(pageOptions.limit)
            totalCount = await result.count()
            result.toArray((err, results) => {
                if (err) {
                    return apiRes.serverError(res)
                }

                client.close();
                return apiRes.success(res, results.length, totalCount, results)
            })


        })
    } catch (err) {
        console.error(err.message);
        return res.status(500).json({ msg: 'Failed', data: [] })
    }

});


module.exports = router;