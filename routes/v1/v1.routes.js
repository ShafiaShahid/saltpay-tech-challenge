import express from 'express'
import request from 'request-promise'

import config from '../../config/config'
import productRoutes from './products.routes'

const router = express.Router()

router.use('/products', productRoutes)

// @route    GET api/countries
// @desc     Get all countries
// @access   public
router.get('/countries', async (req, res) => {
    request(`${config.OFF_URL}/countries.json`)
        .then(result => {
            res.json(JSON.parse(result))
        })
})


export default router;